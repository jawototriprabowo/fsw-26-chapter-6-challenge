const { user_game_history } = require('../models');

module.exports = {
    list: async (req,res) => {
        try {
            const data = await user_game_history.findAll();
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req,res) => {
        try {
            const data = await user_game_history.create({
                name: req.body.name,
                rating: req.body.rating,
                poin: req.body.poin
            });
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await user_game_history.update({
                name: req.body.name,
                rating: req.body.rating,
                poin: req.body.poin
            }, {
                where : {
                    id: req.body.id
                }
            }
            );
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req,res) => {
        try {
            const data = await user_game_history.destroy({
                where : {
                    id: req.body.id
                }
            }
            );
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

}