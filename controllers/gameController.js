const { User_Game } = require('../models');
const user_game_biodata = require('../models/user_game_biodata');
const user_game_history = require('../models/user_game_history');

module.exports = {
    list: async (req,res) => {
        try {
            const data = await User_Game.findAll({
                include: [
                    {model:user_game_biodata, as:'user_game_biodata'},
                    {model:user_game_history, as:'user_game_history'}
                ]
            });
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req,res) => {
        try {
            const data = await User_Game.create({
                name: req.body.name
            });
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await user.update({
                name: req.body.name
            }, {
                where : {
                    id: req.body.id
                }
            }
            );
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req,res) => {
        try {
            const data = await user.destroy({
                where : {
                    id: req.body.id
                }
            }
            );
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },

}